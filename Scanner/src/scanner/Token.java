/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package scanner;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author Adrián
 */
public class Token {
    
    private String lexema;
    private String tipoToken;
    private ArrayList<String> lineas;
    
    public ArrayList<String> getLineas() {
        return lineas;
    }

    public void setLineas(ArrayList<String> lineas) {
        this.lineas = lineas;
    }
    
    public Token(String lexema, String tipoToken){
        this.lexema = lexema;
        this.tipoToken = tipoToken;
        this.lineas = new ArrayList();
    }

    public String getLexema() {
        return lexema;
    }

    public void setLexema(String lexema) {
        this.lexema = lexema;
    }

    public String getTipoToken() {
        return tipoToken;
    }

    public void setTipoToken(String tipoToken) {
        this.tipoToken = tipoToken;
    }
    
    public String printLines(){
        
        String lines = "";
        Set<String> quipu = new HashSet(lineas);
        for (String key : quipu) {
            lines = lines + key + "(" + Collections.frequency(lineas, key) + ") ";
            
        } 
        
        return lines;
    }
    
    
   
}
