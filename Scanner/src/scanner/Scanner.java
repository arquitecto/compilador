/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package scanner;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author Adrián
 */
public class Scanner {

    private Token token;
    private String tipoToken;

    private ArrayList<String> lexemas;
    private ArrayList<Token> tokens;

    public void runScanner(String file) throws FileNotFoundException, IOException {
        Lexer lexer = new Lexer(new FileReader(file));

        lexemas = new ArrayList();
        tokens = new ArrayList();

        while ((tipoToken = lexer.nextToken()) != null) {
            
            if(tipoToken == "ERROR"){
            System.out.print("\n");
            System.out.println("ERROR LÉXICO: " + lexer.yytext() + ", fila " + lexer.getLine() + ", columna " + lexer.getColumn());
            System.out.print("\n");
                    tipoToken = lexer.nextToken();}

            token = new Token(lexer.yytext(), tipoToken);

            if (lexemas.contains(lexer.yytext())) {
               int i = lexemas.indexOf(lexer.yytext());
               tokens.get(i).getLineas().add(lexer.getLine() + "");

            }
            else{
                lexemas.add(lexer.yytext());
                token.getLineas().add(lexer.getLine() + "");
                tokens.add(token);
            }
           
        }
        
        System.out.println("********************  TABLA DE TOKENS ********************");
        tokens.stream().forEach((token1) -> {
            System.out.println(token1.getLexema() + "  ----  " + token1.getTipoToken() + "  ----  " + token1.printLines());
        });

    }

    public static void main(String[] args) throws Exception {

        //String path = "C:\\Users\\Adrián\\Documents\\NetBeansProjects\\Scanner\\src\\Scanner\\Lexer.flex";
        //File file = new File(path);
        //JFlex.Main.generate(file);
        
        Scanner s = new Scanner();
        s.runScanner("C:\\Users\\Adrián\\Documents\\NetBeansProjects\\Scanner\\src\\Scanner\\integral.txt");

    }

}
