/// Prueba de Errores y comentarios
/// Debe reconocer errores de simbolos no reconocidos
/// Comillas sin cerrar, etc.
" un string de 
varias lineas"
int main()
{
  ///  ERRORES DE COMILLAS
  char *x = "ljhgsdkjaghsdf ;
  char y = '\b ';
  char z = '\b ;
  //  Comentarios
  //esto es una prueba de comentarios de linea
  /*
  Prueba de comentarios de Bloque
  "hola soysf "
  char y = '\b ';
  char z = '\b ;  */
  
  ///  ERRORES DE SIMBOLOS NO CONOCIDOS
  \@         //error y error
  Á // error
  int árbol;  // error
  º // error
}
