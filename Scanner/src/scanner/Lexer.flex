/********************* Primera parte: código de usuario *********************/

package scanner;

%%
/************* Segunda parte: declaración de directivas y macros ************/

%unicode
%class Lexer
%type String
%function nextToken
%line
%column

%{
public int getLine(){return yyline;}
%}

%{
public int getColumn(){return yycolumn;}
%}

/* whitespaces*/
LineTerminator = \r|\n|\r\n
InputCharacter = [^\r\n]
WhiteSpace = {LineTerminator} | [ \t\f]

/* comments */
Comment = {TraditionalComment} | {EndOfLineComment} | {DocumentationComment}
TraditionalComment = "/*" [^*] ~"*/" | "/*" "*"+ "/"
EndOfLineComment = "//" {InputCharacter}* {LineTerminator}?
DocumentationComment = "/*" "*"+ [^/*] ~"*/"

/* identifiers */
Identifier = [:jletter:][:jletterdigit:]*

/* integer literals */
DecIntegerLiteral = 0 | [1-9][0-9]*
DecLongLiteral    = {DecIntegerLiteral} [lL]

HexIntegerLiteral = 0 [xX] 0* {HexDigit} {1,8}
HexLongLiteral    = 0 [xX] 0* {HexDigit} {1,16} [lL]
HexDigit          = [0-9a-fA-F]

OctIntegerLiteral = 0+ [1-3]? {OctDigit} {1,15}
OctLongLiteral    = 0+ 1? {OctDigit} {1,21} [lL]
OctDigit          = [0-7]
    
/* floating point literals */        
FloatLiteral  = ({FLit1}|{FLit2}|{FLit3}) {Exponent}? [fF]
DoubleLiteral = ({FLit1}|{FLit2}|{FLit3}) {Exponent}?

FLit1    = [0-9]+ \. [0-9]* 
FLit2    = \. [0-9]+ 
FLit3    = [0-9]+ 
Exponent = [eE] [+-]? [0-9]+

%%
/*************** Tercera parte: declaración de reglas lexicas ***************/

{Comment}                { /* Ignore */ }
{WhiteSpace}             { /* Ignore */ }

"int"                    { return "KEYWORD"; }
"short"                  { return "KEYWORD"; }
"long"                   { return "KEYWORD"; }
"float"                  { return "KEYWORD"; }
"double"                 { return "KEYWORD"; }
"char"                   { return "KEYWORD"; }
"const"                  { return "KEYWORD"; }
"unsigned"               { return "KEYWORD"; }
"auto"                   { return "KEYWORD"; }
"break"			 { return "KEYWORD"; }
"case"			 { return "KEYWORD"; }
"continue"               { return "KEYWORD"; }
"default"	         { return "KEYWORD"; }
"do"			 { return "KEYWORD"; }
"if"			 { return "KEYWORD"; }
"else"			 { return "KEYWORD"; }
"static"                 { return "KEYWORD"; }
"void"			 { return "KEYWORD"; }
"while"			 { return "KEYWORD"; }
"return"		 { return "KEYWORD"; }
"enum"      		 { return "KEYWORD"; }
"extern"      		 { return "KEYWORD"; }
"for"                    { return "KEYWORD"; }
"goto"      		 { return "KEYWORD"; }
"register"               { return "KEYWORD"; }
"signed"      		 { return "KEYWORD"; }
"sizeof"      		 { return "KEYWORD"; }
"read"                   { return "KEYWORD"; }
"struct"      		 { return "KEYWORD"; }
"switch"      		 { return "KEYWORD"; }
"typedef"      		 { return "KEYWORD"; }
"volatile"               { return "KEYWORD"; }
"main"			 { return "KEYWORD"; }
"null"                   { return "KEYWORD"; }

","                      { return "OPERATOR"; }
";"                      { return "OPERATOR"; }
"++"                     { return "OPERATOR"; }
"--"                     { return "OPERATOR"; }
"=="                     { return "OPERATOR"; }
">="                     { return "OPERATOR"; }
">"                      { return "OPERATOR"; }
"?"                      { return "OPERATOR"; }
"<="                     { return "OPERATOR"; }
"<"                      { return "OPERATOR"; }
"!="                     { return "OPERATOR"; }
"||"                     { return "OPERATOR"; }
"&&"                     { return "OPERATOR"; }
"!"                      { return "OPERATOR"; }
"="                      { return "OPERATOR"; }
"+"                      { return "OPERATOR"; }
"-"                      { return "OPERATOR"; }
"*"                      { return "OPERATOR"; }
"/"                      { return "OPERATOR"; }
"%"                      { return "OPERATOR"; }
"("                      { return "OPERATOR"; }
")"                      { return "OPERATOR"; }
"["                      { return "OPERATOR"; }
"]"                      { return "OPERATOR"; }
"{"                      { return "OPERATOR"; }
"}"                      { return "OPERATOR"; }
":"                      { return "OPERATOR"; }
"."                      { return "OPERATOR"; }
"+="                     { return "OPERATOR"; }
"-="                     { return "OPERATOR"; }
"*="                     { return "OPERATOR"; }
"/="                     { return "OPERATOR"; }
"&"                      { return "OPERATOR"; }
"^"                      { return "OPERATOR"; }
"|"                      { return "OPERATOR"; }
">>"                     { return "OPERATOR"; }
"<<"                     { return "OPERATOR"; }
"~"                      { return "OPERATOR"; }
"%="                     { return "OPERATOR"; }
"&="                     { return "OPERATOR"; }
"^="                     { return "OPERATOR"; }
"|="                     { return "OPERATOR"; }
"<<="                    { return "OPERATOR"; }
">>="                    { return "OPERATOR"; }
"->"                     { return "OPERATOR"; }

{Identifier}             { return "IDENTIFIER"; }

{DecIntegerLiteral}      { return "INTEGER_LITERAL"; }
{DecLongLiteral}         { return "INTEGER_LITERAL"; }
  
{HexIntegerLiteral}      { return "INTEGER_LITERAL"; }
{HexLongLiteral}         { return "INTEGER_LITERAL"; }
 
{OctIntegerLiteral}      { return "INTEGER_LITERAL"; }
{OctLongLiteral}         { return "INTEGER_LITERAL"; }

{FloatLiteral}           { return "FLOATING_POINT_LITERAL"; }
{DoubleLiteral}          { return "FLOATING_POINT_LITERAL"; }
{DoubleLiteral}[dD]      { return "FLOATING_POINT_LITERAL"; }


.|\n                     { return "ERROR"; }